# frozen_string_literal: true

require 'yaml'
require 'json'
require 'mongo'

module ObserveChat
  KEYWORDS = %i[nick message time channel server].freeze

  # DatabaseWrapper
  # Wraps the Ruby MongoDB driver into a more intuitive class.
  class DatabaseWrapper
    def initialize(settings)
      client = if settings['conn_string'] != ''
                 Mongo::Client.new(settings['conn_string'],
                                   database: settings['db'])
               else
                 Mongo::Client.new(["#{settings['host']}:#{settings['port']}"],
                                   database: settings['db'])
               end

      @collection = client.database.collection(settings['collection'])
    end

    def save_message(msg, server)
      @collection.insert_one(
        nick: msg.user.nick,
        message: msg.message,
        time: msg.time,
        channel: msg.channel.to_s,
        server: server
      )
    end

    def print_messages
      all.each { |row| puts row.inspect }
    end

    def all
      @collection.find
    end

    def clear
      @collection.drop
    end

    def find(params)
      @collection.find(DatabaseWrapper.filter(params)).find.to_a.to_json
    end

    def self.filter(hash)
      f = hash.select { |k, _| KEYWORDS.include? k.to_sym }
      f = f.each_with_object({}) { |(k, v), m| m[k.to_sym] = v }
      from_c = hash.include?(:from)
      to_c = hash.include?(:to)
      if from_c || to_c
        t = {}
        t[:$gte] = DateTime.parse(hash[:from]) if from_c
        t[:$lte] = DateTime.parse(hash[:to]) if to_c
        f[:time] = t
      end
      f
    end
  end
end
