# frozen_string_literal: true

require_relative '../database/database_wrapper'

module ObserveChat
  # Logger
  # Logs users, channels, networks, messages and other info.
  # Message processing should be done here, not on the bot itself.
  class ChatLogger
    def initialize(settings)
      @db = DatabaseWrapper.new(settings)
    end

    def log(msg, server)
      @db.save_message(msg, server)
    end

    def find(params)
      @db.find(params)
    end
  end
end
