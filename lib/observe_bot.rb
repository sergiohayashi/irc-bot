# frozen_string_literal: true

require 'cinch'
require_relative 'chat_logger'

module ObserveChat
  # ObserveBot
  # The bot itself.
  class ObserveBot < Cinch::Bot
    def initialize(server, channels, bot_name, db_cfg)
      super()
      @chat_logger = ChatLogger.new(db_cfg)

      @config.server = server
      @config.channels = channels
      @config.nick = bot_name
      @config.user = bot_name
      @config.realname = bot_name

      msg_logger = @chat_logger
      on :channel do |msg|
        msg_logger.log(msg, server)
      end
    end

    def find(params)
      @chat_logger.find(params)
    end
  end
end
