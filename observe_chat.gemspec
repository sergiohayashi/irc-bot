# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'observe_chat/version'

Gem::Specification.new do |spec|
  spec.name          = 'observe_chat'
  spec.version       = ObserveChat::VERSION
  spec.authors       = ['Rodrigo Siqueira']
  spec.email         = ['rodrigosiqueiramelo@gmail.com']
  spec.summary       = 'Bot for following open channels'
  spec.description   = 'This is a bot for following open channels interactions'\
    ' with the goal of extract data.'
  spec.license       = 'LGPLv3'

  spec.files         = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      f.match(%r{^(test|spec|features)/})
    end
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'cinch', '~> 2.3.3'
  spec.add_development_dependency 'json', '~> 2.2.0'
  spec.add_development_dependency 'mongo', '~> 2.8.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.68.0'
  spec.add_development_dependency 'rubocop-performance', '~> 1.3.0'
  spec.add_development_dependency 'sinatra', '~> 2.0.5'
  spec.add_development_dependency 'sinatra-cross_origin'
end
