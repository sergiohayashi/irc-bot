#!/usr/bin/env ruby
# frozen_string_literal: true

require 'optparse'
require 'ostruct'
require 'pp'

require_relative '../lib/observe_chat'

# CLIParserObserveChat
# Parses arguments from the CLI to the IRC bot.
class CLIParserObserveChat
  Version = ObserveChat::VERSION

  # CLIOptions
  # Options for command line arguments.
  class CLIOptions
    attr_accessor :source, :start_observer, :setting_path, :version

    def initialize
      self.source = ARGV[0]
      self.start_observer = false
      self.setting_path = './observe_chat.yml'
    end

    def define_options(parser)
      parser.banner = 'Usage: observe_chat [OPTIONS]'
      parser.separator('')
      parser.separator('Specific options:')

      # Options
      start_option(parser)
      setting_path_option(parser)

      # Other common options
      parser.separator('')
      parser.separator('Common Options:')

      # No argument, shows at tail
      parser.on_tail('-h', '--help', 'Show this message') do
        puts parser
        exit
      end

      parser.on_tail('--version', 'Show version') do
        self.version = Version
      end
    end

    def start_option(parser)
      parser.on('-s', '--start', 'Start observe chats') do
        self.start_observer = true
      end
    end

    def setting_path_option(parser)
      parser.on('--setting [PATH]', String, 'Load settings') do |path|
        self.setting_path = path
      end
    end
  end

  attr_reader :parser, :options

  def self.parse(args)
    @options = CLIOptions.new
    @args = OptionParser.new do |parser|
      @options.define_options(parser)
      begin
        parser.parse!(args)
      rescue OptionParser::MissingArgument, OptionParser::InvalidOption => e
        puts e.message
        puts "Try 'observe_chat --help' for more information."
      end
    end
    @options
  end
end

def init_app
  app = ObserveChat::ObserveChat.new!
  app.load_setting('examples/observe_chat.yml')
  app.configure_bot
  app.start_observer
  app
end

def main
  options = CLIParserObserveChat.parse(ARGV)

  return unless options.start_observer

  app = ObserveChat::ObserveChat.new!
  puts options.setting_path
  app.load_setting(options.setting_path)
  app.configure_bot
  app.start_observer
end

main if $PROGRAM_NAME == __FILE__
