require_relative '../lib/settings.rb'
require_relative '../lib/observe_bot.rb'

def creates_instance_of_bot(bot)
  it 'creates instance of bot' do
    expect(bot).to_not be_nil
    expect(bot).to be_instance_of ObserveChat::ObserveBot
  end
end

def has_the_basic_methods(bot)
  it 'basic methods' do
    methods = %i[find]
    methods.each do |met|
      expect(bot).to respond_to(met)
    end
  end
end

RSpec.describe ObserveChat::ObserveBot do
  options = ObserveChat::Settings.parse('./examples/observe_chat.yml')
  server = options.irc.first[0]
  channels = options.irc.first[1]
  bot_name = options.settings('bot_name')
  bot = ObserveChat::ObserveBot.new(server, channels, bot_name,
                                    options.database)
  creates_instance_of_bot(bot)
  has_the_basic_methods(bot)
end
