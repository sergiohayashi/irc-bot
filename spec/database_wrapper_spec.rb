require_relative '../lib/settings'
require_relative '../lib/message'
require_relative '../database/database_wrapper'

def creates_instance_of_database(db)
  it 'creates instance of database' do
    expect(db).to_not be_nil
    expect(db).to be_instance_of ObserveChat::DatabaseWrapper
  end
end

def has_the_basic_methods(db)
  it 'basic methods' do
    methods = %i[save_message print_messages all clear find]
    methods.each do |met|
      expect(db).to respond_to(met)
    end
  end
end

def saves_a_message(db)
  it 'saves a message' do
    msg = ObserverChat::Message.new('john', 'hello', Time.now, '#flusp',
                                    'freenode')
    db.save_message msg, 'freenode'
    expect(db.all.count).to be_eql(1)
  end
end

def finds_a_message(db)
  it 'finds a message' do
    expect(db.find('nick' => 'mike')).to be_eql('[]')
    expect(db.find('nick' => 'john')).to_not be_eql('[]')
  end
end

def returns_messages_with_the_correct_id_format(db)
  it 'returns messages with the correct id format' do
    array_of_strings = db.find('nick' => 'john').split(',')
    id = array_of_strings[0]
    expect(id).to match(/\[{"_id":{"\$oid":"[0-9a-f]{24}"}/)
  end
end

def returns_messages_with_the_correct_nick_format(db)
  it 'returns messages with the correct nick format' do
    array_of_strings = db.find('nick' => 'john').split(',')
    nick = array_of_strings[1]
    expect(nick).to match(/"nick":".*"/)
  end
end

def returns_messages_with_the_correct_message_format(db)
  it 'returns messages with the correct message format' do
    array_of_strings = db.find('nick' => 'john').split(',')
    message = array_of_strings[2]
    expect(message).to match(/"message":".*"/)
  end
end

def returns_messages_with_the_correct_time_format(db)
  it 'returns messages with the correct time format' do
    array_of_strings = db.find('nick' => 'john').split(',')
    time = array_of_strings[3]
    expect(time).to match(/"time":"[0-9]{4}-[01][0-9]-[0-3][0-9] [0-2][0-9]:[0-6
                          ][0-9]:[0-6][0-9] UTC"/)
  end
end

def returns_messages_with_the_correct_channel_format(db)
  it 'returns messages with the correct channel format' do
    array_of_strings = db.find('nick' => 'john').split(',')
    channel = array_of_strings[4]
    expect(channel).to match(/"channel":"#.*"/)
  end
end

def returns_messages_with_the_correct_server_format(db)
  it 'returns messages with the correct server format' do
    array_of_strings = db.find('nick' => 'john').split(',')
    server = array_of_strings[5]
    expect(server).to match(/"server":".*"/)
  end
end

RSpec.describe ObserveChat::DatabaseWrapper do
  opts = ObserveChat::Settings.parse('examples/tester_db.yml').database
  db = ObserveChat::DatabaseWrapper.new(opts)
  creates_instance_of_database(db)
  db.clear
  has_the_basic_methods(db)
  saves_a_message(db)
  finds_a_message(db)
  returns_messages_with_the_correct_id_format(db)
  returns_messages_with_the_correct_nick_format(db)
  returns_messages_with_the_correct_message_format(db)
  returns_messages_with_the_correct_time_format(db)
  returns_messages_with_the_correct_channel_format(db)
  returns_messages_with_the_correct_server_format(db)
end
