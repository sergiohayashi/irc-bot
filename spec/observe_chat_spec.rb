# frozen_string_literal: true

require 'sinatra/base'

require_relative '../lib/observe_chat/version'
require_relative '../lib/observe_chat.rb'

def version_number
  it 'has a version number' do
    expect(ObserveChat::VERSION).not_to be nil
  end
end

def creates_instance_of_obs(obs)
  it 'creates instance of ObserveChat' do
    expect(obs).to_not be_nil
    expect(obs).to be_instance_of ObserveChat::ObserveChat
  end
end

def has_the_basic_methods(obs)
  it 'basic methods' do
    methods = %i[load_setting configure_bot start_observer]
    methods.each do |met|
      expect(obs).to respond_to(met)
    end
  end
end

def load_settings_server(options)
  it 'correctly loads the server' do
    expect(options.irc.first[0]).to match(/irc./)
  end
end

def load_settings_channels(options)
  it 'correctly loads the channels' do
    expect(options.irc.first[1]).to be_instance_of Array
    expect(options.irc.first[1][0]).to match(/#./)
  end
end

def load_settings_bot_name(options)
  it 'correctly loads the bot name' do
    expect(options.settings('bot_name')).to match(/./)
  end
end

def configures_bot_server(bot)
  it 'configure the bot server' do
    expect(bot.config.server).to be_eql('irc.freenode.net')
  end
end

def configures_bot_channels(bot)
  it 'configure the bot channels' do
    expect(bot.config.channels).to be_instance_of Array
    expect(bot.config.channels[0]).to be_eql('#flusp')
  end
end

def configures_bot_name(bot)
  it 'configure the bot name' do
    expect(bot.config.nick).to be_eql('msfloss')
  end
end

def bot_channel_connection(bot)
  it 'checks if the bot connects with a server' do
    bot.start
  end
end

# create the bot with the necessary parameter for testing
def create_test_bot(server, channels, bot_name, database)
  bot = ObserveChat::ObserveBot.new(server, channels, bot_name, database) do
    configure do |c|
      c.nick = 'ConnectionBot'
      c.server = 'irc.freenode.org'
      c.channels = ['#flusp']
    end
    on :connect do
      bot.quit
    end
  end
end

RSpec.describe ObserveChat::ObserveChat do
  obs = ObserveChat::ObserveChat.new!
  version_number
  creates_instance_of_obs(obs)
  has_the_basic_methods(obs)
  options = ObserveChat::Settings.parse('./examples/observe_chat.yml')
  load_settings_server(options)
  load_settings_channels(options)
  load_settings_bot_name(options)
  server = options.irc.first[0]
  channels = options.irc.first[1]
  bot_name = options.settings('bot_name')
  bot = create_test_bot(server, channels, bot_name, options.database)
  configures_bot_server(bot)
  configures_bot_channels(bot)
  configures_bot_name(bot)
  bot_channel_connection(bot)
end
